<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06.04.15
 * Time: 20:11
 */

namespace Dnahrebecki\Stock\Model;

class Store
{
    /** @var integer */
    protected $id;

    /** @var string */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Store
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Store
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}