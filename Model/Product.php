<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06.04.15
 * Time: 20:11
 */

namespace Dnahrebecki\Stock\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Product
{
    /** @var integer */
    protected $id;

    /** @var string */
    protected $name;

    /** @var Category[] */
    protected $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}