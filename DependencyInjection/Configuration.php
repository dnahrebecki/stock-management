<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 06.04.15
 * Time: 20:00
 */

namespace Dnahrebecki\Stock\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        // @todo Implement getConfigTreeBuilder() method.
        // @todo specify event listeners for events
    }
}